qemu-kvm:
  pkg.installed

libvirt-daemon-system:
  pkg.installed

python-libvirt:
  pkg.installed

python3-libvirt:
  pkg.installed

haveged:
  pkg.installed: []
  service.running:
    - enable: true
    - reload: true

/etc/default/haveged:
  file.managed:
    - mode: "0644"
    - watched_in:
        - service: haveged
    - contents: |
        # Options to pass to haveged:
        #   -w sets low entropy watermark (in bits)
        DAEMON_ARGS="-w 1024"

# TODO:
# * gnutils-bin (for certtool)
# * virt.keys
# * libvirtd
# * libguestfs
# * networking
# * default images
