Virtualization platform
=======================

Generic configuration for the doyoueven.click virtualization platform.

Sets up libvirt/kvm on a Debian host, that can be managed with [salt-virt].

[salt-virt]: https://docs.saltstack.com/en/latest/topics/virt/index.html
